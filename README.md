# VEGA ET1035's I2C IP TESTING CODE
In this project, we are using MPU9250 which is 9-Axis Attitude Gyro Accelerometer Magnetometer Sensor Module. 

## Changes made in demo for using different I2C Peripheral

- [I2C-0 (without Repeated Start)](https://gitlab.com/himanshu-diwane/et1035_i2c_test#i2c-0-without-repeated-start)
- [I2C-1 (without Repeated Start)](https://gitlab.com/himanshu-diwane/et1035_i2c_test#i2c-1-without-repeated-start)
- [I2C-1 (with Repeated Start)](https://gitlab.com/himanshu-diwane/et1035_i2c_test#i2c-1-with-repeated-start)

### I2C-0 (without Repeated Start)

- Create an object `TwoWire Wire(0);` (where '0' parameter to the constructor denotes I2C-0) after including `Wire.h` Header file. 
- Convert all `Wire.read()` data of int datatype to `short` by Type casting.
- Replace `false` parameter in `Wire.endTransmission();` fuction call with `true` OR simple remove the parameter.


#### Code after modification

```c++
/*
   Arduino and MPU6050 Accelerometer and Gyroscope Sensor Tutorial
   by Dejan, https://howtomechatronics.com
*/
#include <Wire.h>
TwoWire Wire(0);
const int MPU = 0x68; // MPU6050 I2C address
float AccX, AccY, AccZ;
float GyroX, GyroY, GyroZ;
float accAngleX, accAngleY, gyroAngleX, gyroAngleY, gyroAngleZ;
float roll, pitch, yaw;
float AccErrorX, AccErrorY, GyroErrorX, GyroErrorY, GyroErrorZ;
float elapsedTime, currentTime, previousTime;
int c = 0;
void setup() {
  Serial.begin(115200);
  Wire.begin();                      // Initialize comunication
  Wire.beginTransmission(MPU);       // Start communication with MPU6050 // MPU=0x68
  Wire.write(0x6B);                  // Talk to the register 6B
  Wire.write(0x00);                  // Make reset - place a 0 into the 6B register
  Wire.endTransmission(true);        //end the transmission
  /*
  // Configure Accelerometer Sensitivity - Full Scale Range (default +/- 2g)
  Wire.beginTransmission(MPU);
  Wire.write(0x1C);                  //Talk to the ACCEL_CONFIG register (1C hex)
  Wire.write(0x10);                  //Set the register bits as 00010000 (+/- 8g full scale range)
  Wire.endTransmission(true);
  // Configure Gyro Sensitivity - Full Scale Range (default +/- 250deg/s)
  Wire.beginTransmission(MPU);
  Wire.write(0x1B);                   // Talk to the GYRO_CONFIG register (1B hex)
  Wire.write(0x10);                   // Set the register bits as 00010000 (1000deg/s full scale)
  Wire.endTransmission(true);
  delay(20);
  */
  // Call this function if you need to get the IMU error values for your module
  calculate_IMU_error();
  delay(20);
}
void loop() {
  // === Read acceleromter data === //
  Wire.beginTransmission(MPU);
  Wire.write(0x3B); // Start with register 0x3B (ACCEL_XOUT_H)
  Wire.endTransmission();
  Wire.requestFrom(MPU, 6, true); // Read 6 registers total, each axis value is stored in 2 registers
  //For a range of +-2g, we need to divide the raw values by 16384, according to the datasheet
  AccX = (short)(Wire.read() << 8 | Wire.read()) / 16384.0; // X-axis value
  AccY = (short)(Wire.read() << 8 | Wire.read()) / 16384.0; // Y-axis value
  AccZ = (short)(Wire.read() << 8 | Wire.read()) / 16384.0; // Z-axis value

  // Calculating Roll and Pitch from the accelerometer data
  accAngleX = (atan(AccY / sqrt(pow(AccX, 2) + pow(AccZ, 2))) * 180 / PI) - 0.58; // AccErrorX ~(0.58) See the calculate_IMU_error()custom function for more details
  accAngleY = (atan(-1 * AccX / sqrt(pow(AccY, 2) + pow(AccZ, 2))) * 180 / PI) + 1.58; // AccErrorY ~(-1.58)
  // === Read gyroscope data === //
  previousTime = currentTime;        // Previous time is stored before the actual time read
  currentTime = millis();            // Current time actual time read
  elapsedTime = (currentTime - previousTime) / 1000; // Divide by 1000 to get seconds
  Wire.beginTransmission(MPU);
  Wire.write(0x43); // Gyro data first register address 0x43
  Wire.endTransmission();
  Wire.requestFrom(MPU, 6, true); // Read 4 registers total, each axis value is stored in 2 registers
  GyroX = (short)(Wire.read() << 8 | Wire.read()) / 131.0; // For a 250deg/s range we have to divide first the raw value by 131.0, according to the datasheet
  GyroY = (short)(Wire.read() << 8 | Wire.read()) / 131.0;
  GyroZ = (short)(Wire.read() << 8 | Wire.read()) / 131.0;

  // Correct the outputs with the calculated error values
  GyroX = (GyroX + 0.56); // GyroErrorX ~(-0.56)
  GyroY = (GyroY - 2); // GyroErrorY ~(2)
  GyroZ = (GyroZ + 0.79); // GyroErrorZ ~ (-0.8)
  // Currently the raw values are in degrees per seconds, deg/s, so we need to multiply by sendonds (s) to get the angle in degrees
  gyroAngleX = gyroAngleX + GyroX * elapsedTime; // deg/s * s = deg
  gyroAngleY = gyroAngleY + GyroY * elapsedTime;
  yaw =  (yaw + GyroZ * elapsedTime);
  // Complementary filter - combine acceleromter and gyro angle values
  roll = (0.96 * gyroAngleX + 0.04 * accAngleX);
  pitch = (0.96 * gyroAngleY + 0.04 * accAngleY);
  
  // Print the values on the serial monitor
  Serial.print(roll);
  Serial.print("/");
  Serial.print(pitch);
  Serial.print("/");
  Serial.println(yaw);
}

void calculate_IMU_error() {
  // We can call this funtion in the setup section to calculate the accelerometer and gyro data error. From here we will get the error values used in the above equations printed on the Serial Monitor.
  // Note that we should place the IMU flat in order to get the proper values, so that we then can the correct values
  // Read accelerometer values 200 times
  while (c < 200) {
    Wire.beginTransmission(MPU);
    Wire.write(0x3B);
    Wire.endTransmission();
    Wire.requestFrom(MPU, 6, true);
    AccX = (short)(Wire.read() << 8 | Wire.read()) / 16384.0 ;
    AccY = (short)(Wire.read() << 8 | Wire.read()) / 16384.0 ;
    AccZ = (short)(Wire.read() << 8 | Wire.read()) / 16384.0 ;
    // Sum all readings
    AccErrorX = AccErrorX + ((atan((AccY) / sqrt(pow((AccX), 2) + pow((AccZ), 2))) * 180 / PI));
    AccErrorY = AccErrorY + ((atan(-1 * (AccX) / sqrt(pow((AccY), 2) + pow((AccZ), 2))) * 180 / PI));
    c++;
  }
  //Divide the sum by 200 to get the error value
  AccErrorX = AccErrorX / 200;
  AccErrorY = AccErrorY / 200;
  c = 0;
  // Read gyro values 200 times
  while (c < 200) {
    Wire.beginTransmission(MPU);
    Wire.write(0x43);
    Wire.endTransmission();
    Wire.requestFrom(MPU, 6, true);
    GyroX = (short)(Wire.read() << 8 | Wire.read());
    GyroY = (short)(Wire.read() << 8 | Wire.read());
    GyroZ = (short)(Wire.read() << 8 | Wire.read());
    // Sum all readings
    GyroErrorX = GyroErrorX + (GyroX / 131.0);
    GyroErrorY = GyroErrorY + (GyroY / 131.0);
    GyroErrorZ = GyroErrorZ + (GyroZ / 131.0);
    c++;
  }
  //Divide the sum by 200 to get the error value
  GyroErrorX = GyroErrorX / 200;
  GyroErrorY = GyroErrorY / 200;
  GyroErrorZ = GyroErrorZ / 200;
  // Print the error values on the Serial Monitor
  Serial.print("AccErrorX: ");
  Serial.println(AccErrorX);
  Serial.print("AccErrorY: ");
  Serial.println(AccErrorY);
  Serial.print("GyroErrorX: ");
  Serial.println(GyroErrorX);
  Serial.print("GyroErrorY: ");
  Serial.println(GyroErrorY);
  Serial.print("GyroErrorZ: ");
  Serial.println(GyroErrorZ);
}
```

### I2C-1 (without Repeated Start)

- Add `TwoWire Wire(1);` line after including `Wire.h` Header file. 
- Typecast `Wire.read()` data to `short` datatype.
- Replace `false` parameter in `Wire.endTransmission();` calling function with `true` OR simple remove the parameter.


#### Code after modification

```c++
/*
   Arduino and MPU6050 Accelerometer and Gyroscope Sensor Tutorial
   by Dejan, https://howtomechatronics.com
*/
#include <Wire.h>
TwoWire Wire(1);
const int MPU = 0x68; // MPU6050 I2C address
float AccX, AccY, AccZ;
float GyroX, GyroY, GyroZ;
float accAngleX, accAngleY, gyroAngleX, gyroAngleY, gyroAngleZ;
float roll, pitch, yaw;
float AccErrorX, AccErrorY, GyroErrorX, GyroErrorY, GyroErrorZ;
float elapsedTime, currentTime, previousTime;
int c = 0;
void setup() {
  Serial.begin(115200);
  Wire.begin();                      // Initialize comunication
  Wire.beginTransmission(MPU);       // Start communication with MPU6050 // MPU=0x68
  Wire.write(0x6B);                  // Talk to the register 6B
  Wire.write(0x00);                  // Make reset - place a 0 into the 6B register
  Wire.endTransmission(true);        //end the transmission
  /*
  // Configure Accelerometer Sensitivity - Full Scale Range (default +/- 2g)
  Wire.beginTransmission(MPU);
  Wire.write(0x1C);                  //Talk to the ACCEL_CONFIG register (1C hex)
  Wire.write(0x10);                  //Set the register bits as 00010000 (+/- 8g full scale range)
  Wire.endTransmission(true);
  // Configure Gyro Sensitivity - Full Scale Range (default +/- 250deg/s)
  Wire.beginTransmission(MPU);
  Wire.write(0x1B);                   // Talk to the GYRO_CONFIG register (1B hex)
  Wire.write(0x10);                   // Set the register bits as 00010000 (1000deg/s full scale)
  Wire.endTransmission(true);
  delay(20);
  */
  // Call this function if you need to get the IMU error values for your module
  calculate_IMU_error();
  delay(20);
}
void loop() {
  // === Read acceleromter data === //
  Wire.beginTransmission(MPU);
  Wire.write(0x3B); // Start with register 0x3B (ACCEL_XOUT_H)
  Wire.endTransmission();
  Wire.requestFrom(MPU, 6, true); // Read 6 registers total, each axis value is stored in 2 registers
  //For a range of +-2g, we need to divide the raw values by 16384, according to the datasheet
  AccX = (short)(Wire.read() << 8 | Wire.read()) / 16384.0; // X-axis value
  AccY = (short)(Wire.read() << 8 | Wire.read()) / 16384.0; // Y-axis value
  AccZ = (short)(Wire.read() << 8 | Wire.read()) / 16384.0; // Z-axis value

  // Calculating Roll and Pitch from the accelerometer data
  accAngleX = (atan(AccY / sqrt(pow(AccX, 2) + pow(AccZ, 2))) * 180 / PI) - 0.58; // AccErrorX ~(0.58) See the calculate_IMU_error()custom function for more details
  accAngleY = (atan(-1 * AccX / sqrt(pow(AccY, 2) + pow(AccZ, 2))) * 180 / PI) + 1.58; // AccErrorY ~(-1.58)
  // === Read gyroscope data === //
  previousTime = currentTime;        // Previous time is stored before the actual time read
  currentTime = millis();            // Current time actual time read
  elapsedTime = (currentTime - previousTime) / 1000; // Divide by 1000 to get seconds
  Wire.beginTransmission(MPU);
  Wire.write(0x43); // Gyro data first register address 0x43
  Wire.endTransmission();
  Wire.requestFrom(MPU, 6, true); // Read 4 registers total, each axis value is stored in 2 registers
  GyroX = (short)(Wire.read() << 8 | Wire.read()) / 131.0; // For a 250deg/s range we have to divide first the raw value by 131.0, according to the datasheet
  GyroY = (short)(Wire.read() << 8 | Wire.read()) / 131.0;
  GyroZ = (short)(Wire.read() << 8 | Wire.read()) / 131.0;

  // Correct the outputs with the calculated error values
  GyroX = (GyroX + 0.56); // GyroErrorX ~(-0.56)
  GyroY = (GyroY - 2); // GyroErrorY ~(2)
  GyroZ = (GyroZ + 0.79); // GyroErrorZ ~ (-0.8)
  // Currently the raw values are in degrees per seconds, deg/s, so we need to multiply by sendonds (s) to get the angle in degrees
  gyroAngleX = gyroAngleX + GyroX * elapsedTime; // deg/s * s = deg
  gyroAngleY = gyroAngleY + GyroY * elapsedTime;
  yaw =  (yaw + GyroZ * elapsedTime);
  // Complementary filter - combine acceleromter and gyro angle values
  roll = (0.96 * gyroAngleX + 0.04 * accAngleX);
  pitch = (0.96 * gyroAngleY + 0.04 * accAngleY);
  
  // Print the values on the serial monitor
  Serial.print(roll);
  Serial.print("/");
  Serial.print(pitch);
  Serial.print("/");
  Serial.println(yaw);
}

void calculate_IMU_error() {
  // We can call this funtion in the setup section to calculate the accelerometer and gyro data error. From here we will get the error values used in the above equations printed on the Serial Monitor.
  // Note that we should place the IMU flat in order to get the proper values, so that we then can the correct values
  // Read accelerometer values 200 times
  while (c < 200) {
    Wire.beginTransmission(MPU);
    Wire.write(0x3B);
    Wire.endTransmission();
    Wire.requestFrom(MPU, 6, true);
    AccX = (short)(Wire.read() << 8 | Wire.read()) / 16384.0 ;
    AccY = (short)(Wire.read() << 8 | Wire.read()) / 16384.0 ;
    AccZ = (short)(Wire.read() << 8 | Wire.read()) / 16384.0 ;
    // Sum all readings
    AccErrorX = AccErrorX + ((atan((AccY) / sqrt(pow((AccX), 2) + pow((AccZ), 2))) * 180 / PI));
    AccErrorY = AccErrorY + ((atan(-1 * (AccX) / sqrt(pow((AccY), 2) + pow((AccZ), 2))) * 180 / PI));
    c++;
  }
  //Divide the sum by 200 to get the error value
  AccErrorX = AccErrorX / 200;
  AccErrorY = AccErrorY / 200;
  c = 0;
  // Read gyro values 200 times
  while (c < 200) {
    Wire.beginTransmission(MPU);
    Wire.write(0x43);
    Wire.endTransmission();
    Wire.requestFrom(MPU, 6, true);
    GyroX = (short)(Wire.read() << 8 | Wire.read());
    GyroY = (short)(Wire.read() << 8 | Wire.read());
    GyroZ = (short)(Wire.read() << 8 | Wire.read());
    // Sum all readings
    GyroErrorX = GyroErrorX + (GyroX / 131.0);
    GyroErrorY = GyroErrorY + (GyroY / 131.0);
    GyroErrorZ = GyroErrorZ + (GyroZ / 131.0);
    c++;
  }
  //Divide the sum by 200 to get the error value
  GyroErrorX = GyroErrorX / 200;
  GyroErrorY = GyroErrorY / 200;
  GyroErrorZ = GyroErrorZ / 200;
  // Print the error values on the Serial Monitor
  Serial.print("AccErrorX: ");
  Serial.println(AccErrorX);
  Serial.print("AccErrorY: ");
  Serial.println(AccErrorY);
  Serial.print("GyroErrorX: ");
  Serial.println(GyroErrorX);
  Serial.print("GyroErrorY: ");
  Serial.println(GyroErrorY);
  Serial.print("GyroErrorZ: ");
  Serial.println(GyroErrorZ);
}
```

### I2C-1 (with Repeated Start)

- Create an object `TwoWire Wire(1);` after including `Wire.h` Header file. 
- Typecast `Wire.read()` data to `short` datatype

#### Code after modification

```c++
/*
   Arduino and MPU6050 Accelerometer and Gyroscope Sensor Tutorial
   by Dejan, https://howtomechatronics.com
*/
#include <Wire.h>
TwoWire Wire(1);
const int MPU = 0x68; // MPU6050 I2C address
float AccX, AccY, AccZ;
float GyroX, GyroY, GyroZ;
float accAngleX, accAngleY, gyroAngleX, gyroAngleY, gyroAngleZ;
float roll, pitch, yaw;
float AccErrorX, AccErrorY, GyroErrorX, GyroErrorY, GyroErrorZ;
float elapsedTime, currentTime, previousTime;
int c = 0;
void setup() {
  Serial.begin(115200);
  Wire.begin();                      // Initialize comunication
  Wire.beginTransmission(MPU);       // Start communication with MPU6050 // MPU=0x68
  Wire.write(0x6B);                  // Talk to the register 6B
  Wire.write(0x00);                  // Make reset - place a 0 into the 6B register
  Wire.endTransmission(true);        //end the transmission
  /*
  // Configure Accelerometer Sensitivity - Full Scale Range (default +/- 2g)
  Wire.beginTransmission(MPU);
  Wire.write(0x1C);                  //Talk to the ACCEL_CONFIG register (1C hex)
  Wire.write(0x10);                  //Set the register bits as 00010000 (+/- 8g full scale range)
  Wire.endTransmission(true);
  // Configure Gyro Sensitivity - Full Scale Range (default +/- 250deg/s)
  Wire.beginTransmission(MPU);
  Wire.write(0x1B);                   // Talk to the GYRO_CONFIG register (1B hex)
  Wire.write(0x10);                   // Set the register bits as 00010000 (1000deg/s full scale)
  Wire.endTransmission(true);
  delay(20);
  */
  // Call this function if you need to get the IMU error values for your module
  calculate_IMU_error();
  delay(20);
}
void loop() {
  // === Read acceleromter data === //
  Wire.beginTransmission(MPU);
  Wire.write(0x3B); // Start with register 0x3B (ACCEL_XOUT_H)
  Wire.endTransmission(false);
  Wire.requestFrom(MPU, 6, true); // Read 6 registers total, each axis value is stored in 2 registers
  //For a range of +-2g, we need to divide the raw values by 16384, according to the datasheet
  AccX = (short)(Wire.read() << 8 | Wire.read()) / 16384.0; // X-axis value
  AccY = (short)(Wire.read() << 8 | Wire.read()) / 16384.0; // Y-axis value
  AccZ = (short)(Wire.read() << 8 | Wire.read()) / 16384.0; // Z-axis value

  // Calculating Roll and Pitch from the accelerometer data
  accAngleX = (atan(AccY / sqrt(pow(AccX, 2) + pow(AccZ, 2))) * 180 / PI) - 0.58; // AccErrorX ~(0.58) See the calculate_IMU_error()custom function for more details
  accAngleY = (atan(-1 * AccX / sqrt(pow(AccY, 2) + pow(AccZ, 2))) * 180 / PI) + 1.58; // AccErrorY ~(-1.58)
  // === Read gyroscope data === //
  previousTime = currentTime;        // Previous time is stored before the actual time read
  currentTime = millis();            // Current time actual time read
  elapsedTime = (currentTime - previousTime) / 1000; // Divide by 1000 to get seconds
  Wire.beginTransmission(MPU);
  Wire.write(0x43); // Gyro data first register address 0x43
  Wire.endTransmission(false);
  Wire.requestFrom(MPU, 6, true); // Read 4 registers total, each axis value is stored in 2 registers
  GyroX = (short)(Wire.read() << 8 | Wire.read()) / 131.0; // For a 250deg/s range we have to divide first the raw value by 131.0, according to the datasheet
  GyroY = (short)(Wire.read() << 8 | Wire.read()) / 131.0;
  GyroZ = (short)(Wire.read() << 8 | Wire.read()) / 131.0;

  // Correct the outputs with the calculated error values
  GyroX = (GyroX + 0.56); // GyroErrorX ~(-0.56)
  GyroY = (GyroY - 2); // GyroErrorY ~(2)
  GyroZ = (GyroZ + 0.79); // GyroErrorZ ~ (-0.8)
  // Currently the raw values are in degrees per seconds, deg/s, so we need to multiply by sendonds (s) to get the angle in degrees
  gyroAngleX = gyroAngleX + GyroX * elapsedTime; // deg/s * s = deg
  gyroAngleY = gyroAngleY + GyroY * elapsedTime;
  yaw =  (yaw + GyroZ * elapsedTime);
  // Complementary filter - combine acceleromter and gyro angle values
  roll = (0.96 * gyroAngleX + 0.04 * accAngleX);
  pitch = (0.96 * gyroAngleY + 0.04 * accAngleY);
  
  // Print the values on the serial monitor
  Serial.print(roll);
  Serial.print("/");
  Serial.print(pitch);
  Serial.print("/");
  Serial.println(yaw);
}

void calculate_IMU_error() {
  // We can call this funtion in the setup section to calculate the accelerometer and gyro data error. From here we will get the error values used in the above equations printed on the Serial Monitor.
  // Note that we should place the IMU flat in order to get the proper values, so that we then can the correct values
  // Read accelerometer values 200 times
  while (c < 200) {
    Wire.beginTransmission(MPU);
    Wire.write(0x3B);
    Wire.endTransmission(false);
    Wire.requestFrom(MPU, 6, true);
    AccX = (short)(Wire.read() << 8 | Wire.read()) / 16384.0 ;
    AccY = (short)(Wire.read() << 8 | Wire.read()) / 16384.0 ;
    AccZ = (short)(Wire.read() << 8 | Wire.read()) / 16384.0 ;
    // Sum all readings
    AccErrorX = AccErrorX + ((atan((AccY) / sqrt(pow((AccX), 2) + pow((AccZ), 2))) * 180 / PI));
    AccErrorY = AccErrorY + ((atan(-1 * (AccX) / sqrt(pow((AccY), 2) + pow((AccZ), 2))) * 180 / PI));
    c++;
  }
  //Divide the sum by 200 to get the error value
  AccErrorX = AccErrorX / 200;
  AccErrorY = AccErrorY / 200;
  c = 0;
  // Read gyro values 200 times
  while (c < 200) {
    Wire.beginTransmission(MPU);
    Wire.write(0x43);
    Wire.endTransmission(false);
    Wire.requestFrom(MPU, 6, true);
    GyroX = (short)(Wire.read() << 8 | Wire.read());
    GyroY = (short)(Wire.read() << 8 | Wire.read());
    GyroZ = (short)(Wire.read() << 8 | Wire.read());
    // Sum all readings
    GyroErrorX = GyroErrorX + (GyroX / 131.0);
    GyroErrorY = GyroErrorY + (GyroY / 131.0);
    GyroErrorZ = GyroErrorZ + (GyroZ / 131.0);
    c++;
  }
  //Divide the sum by 200 to get the error value
  GyroErrorX = GyroErrorX / 200;
  GyroErrorY = GyroErrorY / 200;
  GyroErrorZ = GyroErrorZ / 200;
  // Print the error values on the Serial Monitor
  Serial.print("AccErrorX: ");
  Serial.println(AccErrorX);
  Serial.print("AccErrorY: ");
  Serial.println(AccErrorY);
  Serial.print("GyroErrorX: ");
  Serial.println(GyroErrorX);
  Serial.print("GyroErrorY: ");
  Serial.println(GyroErrorY);
  Serial.print("GyroErrorZ: ");
  Serial.println(GyroErrorZ);
}
```

## Changes to made in BSP

We need to update 5 files
- Wire.cpp      [ /home/himanshu/.arduino15/packages/vega/hardware/riscv/1.0.6/libraries/Wire/src/Wire.cpp ]
- platform.h    [ /home/himanshu/.arduino15/packages/vega/hardware/riscv/1.0.6/thejas32/include/platform.h ]
- link.lds      [ /home/himanshu/.arduino15/packages/vega/hardware/riscv/1.0.6/thejas32/link.lds ]
- wiring.c      [ /home/himanshu/.arduino15/packages/vega/hardware/riscv/1.0.6/cores/arduino/wiring.c ]
- UARTClass.cpp [ /home/himanshu/.arduino15/packages/vega/hardware/riscv/1.0.6/cores/arduino/UARTClass.cpp ]

### 1. Wire.cpp 
In Wire libray few changes are made for repeated start.

#### Code
```c++

/* **************************************************************************

 Copyright (C) 2020  CDAC(T). All rights reserved.

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.

 ************************************************************************** */

/*
 @file Wire.cpp
 @brief Contains routines for different I2C functions
 @detail
 */

/*  Include section
 *
 ***************************************************/
extern "C" {
#include <stdlib.h>
#include <string.h>
#include <inttypes.h>
#include "utility/twi.h"
}

#include "Wire.h"

/*  Global variable section
 *
 ***************************************************/
uint8_t TwoWire::rxBuffer[BUFFER_LENGTH];
uint8_t TwoWire::rxBufferIndex = 0;
uint8_t TwoWire::rxBufferLength = 0;

uint8_t TwoWire::slaveAddress = 0;
uint8_t TwoWire::txBuffer[BUFFER_LENGTH];
uint8_t TwoWire::txBufferIndex = 0;
uint8_t TwoWire::txBufferLength = 0;

uint8_t TwoWire::transmitting = 0;
uint8_t DataRead;

/** @fn TwoWire::TwoWire(uint8_t _id): id(_id)
 @brief Initialize i2c Port.
 @details This function initialize the I2C port.
 @warning
 @param[in]  unsigned int _id: This parameter sets I2C Port.
 '0' = I2C PORT 0, '1' = I2C PORT 1
 @param[Out] No output parameter
 */

TwoWire::TwoWire(uint8_t _id) :
		id(_id) {

}

/**
 @fn void TwoWire::begin(void)
 @brief I2C Initialization
 @details Initializes I2C Clock period registers
 @param[in]  No input parameter
 @param[Out] No output parameter
 @return Void function.
 */

void TwoWire::begin(void) {
	rxBufferIndex = 0;
	rxBufferLength = 0;
	txBufferIndex = 0;
	txBufferLength = 0;

	IIC_REG(id, IIC_REG_TxCLR) = 0xFF; //clear TxFIFO register
	__asm__ __volatile__ ("fence");
	IIC_REG(id, IIC_REG_CHL) = 0xC8; //set timing registers CHL,CHH,CHHL,CHHH
	IIC_REG(id, IIC_REG_CHH) = 0x00;
	// unsigned short CHH = F_CPU / (4 * I2C_CLK);
	IIC_REG(id, IIC_REG_CHHL) = 0x64;
	IIC_REG(id, IIC_REG_CHHH) = 0x00;
	__asm__ __volatile__ ("fence");

}
void TwoWire::begin(uint8_t address) {
	begin();
}
void TwoWire::begin(int address) {
	begin((uint8_t) address);
}
/**
 @fn void TwoWire::setClock(uint32_t clock)
 @brief Configures I2C
 @details Configures I2C Clock period registers
 @param[in] (uint32_t clock)-- I2C clock frequency to be used
 @param[Out] No output parameters.
 @return nil
 */

void TwoWire::setClock(uint32_t clock) {
	IIC_REG(id, IIC_REG_CHL) = 0xC8; //set timing registers CHL,CHH,CHHL,CHHH
	IIC_REG(id, IIC_REG_CHH) = 0x00;
	// unsigned short CHH = F_CPU / (4 * I2C_CLK);
	IIC_REG(id, IIC_REG_CHHL) = 0x64;
	IIC_REG(id, IIC_REG_CHHH) = 0x00;
	__asm__ __volatile__ ("fence");
}

uint8_t TwoWire::requestFrom(uint8_t address, uint8_t quantity,
		uint32_t iaddress, uint8_t isize, uint8_t sendStop) { //address = slave adress,quantity=num ofbytes
	return requestFrom((uint8_t) address, (uint8_t) quantity,
			(uint8_t) sendStop);
}
/**
 @fn uint8_t TwoWire::requestFrom(uint8_t address, uint8_t quantity,uint8_t sendStop)
 @brief to request bytes from a peripheral device.
 @details allows one master device to send multiple requests while in control. The default value is true.
 @param[in] (uint8_t address)--7-bit slave address of the device to request bytes from.
 @param[in] (uint8_t quantity)--the number of bytes to request.
 @param[in] (uint8_t sendStop)--true or false.
 @param[Out] No output parameters.
 @return rxBufferLength if true and return 0 when NACK is received
 */

uint8_t TwoWire::requestFrom(uint8_t address, uint8_t quantity,
		uint8_t sendStop) {
	rxBufferLength = quantity;
	IIC_REG(id, IIC_REG_CR ) = ((quantity << 2) | 0x01); //Set Start bit for read,set read length
	__asm__ __volatile__ ("fence");
	slaveAddress = ((address << 1) | IIC_READ);	//shift address to indicate R/W
	while (((IIC_REG(id, IIC_REG_SR0)) & IIC_SR0_TxFFF) == (IIC_SR0_TxFFF))
		; //TxFF full or not
	IIC_REG(id, IIC_REG_TxFF) = slaveAddress; //write slave address to Txff
	__asm__ __volatile__ ("fence");

	while (((IIC_REG(id, IIC_REG_SR0)) & (IIC_SR0_TxC)) != (IIC_SR0_TxC))
		; //Transmission is completed or not
	if (((IIC_REG(id, IIC_REG_SR1)) & (IIC_SR1_NACK)) == (IIC_SR1_NACK)) { //checks NACK
		while (((IIC_REG(id, IIC_REG_SR0)) & (IIC_SR0_STPS)) != (IIC_SR0_STPS))
			; //wait for stop bit to be set
		return 0;
	}
	while (((IIC_REG(id, IIC_REG_SR0)) & (IIC_SR0_RxC)) != (IIC_SR0_RxC))
		; //Rxc or not
	if (rxBufferLength > BUFFER_LENGTH) {
		Serial.print("data too long to fit in transmit buffer for read. \n\r");
		return 0;	//data too long to fit in transmit buffer.
	}
	for (int i = 0; i < rxBufferLength; i++) {
		while (((IIC_REG(id, IIC_REG_SR0)) & (IIC_SR0_RxFFE)) == (IIC_SR0_RxFFE))
			; //check RxFF empty
		rxBuffer[i] = IIC_REG(id, IIC_REG_RxFF); //read data from Rx register
	}
	// while (((IIC_REG(id, IIC_REG_SR0)) & (IIC_SR0_STPS)) != IIC_SR0_STPS)
	// 	; //wait for stop bit to be set
	

	if (sendStop) {
		IIC_REG(id,IIC_REG_CR) = 0x02; 	//set stop bit
		__asm__ __volatile__ ("fence");
		while (((IIC_REG(id, IIC_REG_SR0)) & IIC_SR0_STPS) != (IIC_SR0_STPS))
			; //check stop sequence initiated
	}

	rxBufferIndex = 0;
	return rxBufferLength;
}

uint8_t TwoWire::requestFrom(uint8_t address, uint8_t quantity) {
	return requestFrom((uint8_t) address, (uint8_t) quantity, (uint8_t) true);
}

uint8_t TwoWire::requestFrom(int address, int quantity) {
	return requestFrom((uint8_t) address, (uint8_t) quantity, (uint8_t) true);
}

uint8_t TwoWire::requestFrom(int address, int quantity, int sendStop) {

	return requestFrom((uint8_t) address, (uint8_t) quantity,
			(uint8_t) sendStop);
}
/**
 @fn void TwoWire::beginTransmission(uint8_t address)
 @brief begins transmission
 @details begins a transmission to the I2C peripheral device with the given address
 @param[in] (uint8_t address)--the 7-bit address of the device to transmit to.
 @param[Out] No output parameters.
 @return nil
 */
void TwoWire::beginTransmission(uint8_t address) {

	// indicate that we are transmitting
	transmitting = 1;
	// set address of targeted slave
	slaveAddress = (address << 1) | IIC_WRITE;	//shift address to indicate R/W
	// reset tx buffer iterator vars
	txBufferIndex = 0;
	txBufferLength = 0;
	IIC_REG(id,IIC_REG_TxCLR) = 0xFF;		//clear TxFifo
	while ((((IIC_REG(id, IIC_REG_SR0)) & IIC_SR0_TxFFE) != (IIC_SR0_TxFFE))
			|| (((IIC_REG(id, IIC_REG_SR0)) & IIC_SR0_TxC) != (IIC_SR0_TxC)))
		;		//checks for transmission complete and TxFifo empty
	IIC_REG(id,IIC_REG_CR) = 0x01;		//Set Start bit for write
	__asm__ __volatile__ ("fence");
	while (((IIC_REG(id, IIC_REG_SR0)) & IIC_SR0_STAS) != (IIC_SR0_STAS))
		;		//check start sequence initiated

}

void TwoWire::beginTransmission(int address) {
	beginTransmission((uint8_t) address);
}
/**
 @fn uint8_t TwoWire::endTransmission(uint8_t sendStop)
 @brief  ends a transmission to a peripheral device
 @details This function ends a transmission to a peripheral device that was
 begun by beginTransmission() and transmits the bytes that were queued by write()
 @param[in] (uint8_t sendStop)--true or false. True will send a stop message, releasing
 the bus after transmission. False will send a restart, keeping the connection active.
 @param[Out] No output parameters.
 @return
 0: success.
 1: data too long to fit in transmit buffer.
 2: received NACK on transmit of address.
 3: received NACK on transmit of data.
 4: other error.
 5: timeout
 */

uint8_t TwoWire::endTransmission(uint8_t sendStop) {
	while (((IIC_REG(id, IIC_REG_SR0)) & IIC_SR0_TxFFF) == ( IIC_SR0_TxFFF))
		; //TxFF full or not
	IIC_REG(id,IIC_REG_TxFF) = slaveAddress; //write slave address to Txff
	// slaveAddress++;
	__asm__ __volatile__ ("fence");
	while (((IIC_REG(id, IIC_REG_SR0)) & IIC_SR0_TxC) != (IIC_SR0_TxC))
		;		//Transmission is completed or not

	if (((IIC_REG(id, IIC_REG_SR1)) & IIC_SR1_NACK) == (IIC_SR1_NACK)) { //checks NACK

		while (((IIC_REG(id, IIC_REG_SR0)) & IIC_SR0_STPS) != (IIC_SR0_STPS))
			;			//wait for stop bit to be set
		return 2;			//received NACK on transmit of address.
	}
	while (((IIC_REG(id, IIC_REG_SR0)) & IIC_SR0_TxFFF) == (IIC_SR0_TxFFF))
		;			//TxFF full or not
	if (txBufferLength > BUFFER_LENGTH) {
		Serial.print("data too long to fit in transmit buffer in write. \n\r");
		return 1;	//data too long to fit in transmit buffer.
	}
	for (int i = 0; i < txBufferLength; i++) {

		IIC_REG(id,IIC_REG_TxFF) = txBuffer[i];	//data of write stored in buffer in write()

		while (((IIC_REG(id, IIC_REG_SR0)) & IIC_SR0_TxC) != (IIC_SR0_TxC))
			;			//Transmission is completed or not

		if (((IIC_REG(id, IIC_REG_SR1)) & IIC_SR1_NACK) == (IIC_SR1_NACK)) { //checks NACK
			Serial.print("NACK rxd and stop bit set in write \n\r");
			while (((IIC_REG(id, IIC_REG_SR0)) & IIC_SR0_STPS) != (IIC_SR0_STPS))
				; //wait for stop bit to be set
			return 3;				// received NACK on transmit of data.
		}
	}

	if (sendStop == false) {
		transmitting = 1;
		// reset tx buffer iterator vars
		txBufferIndex = 0;
		txBufferLength = 0;
		IIC_REG(id,IIC_REG_TxCLR) = 0xFF;		//clear TxFifo
		while ((((IIC_REG(id, IIC_REG_SR0)) & IIC_SR0_TxFFE) != (IIC_SR0_TxFFE))
				|| (((IIC_REG(id, IIC_REG_SR0)) & IIC_SR0_TxC) != (IIC_SR0_TxC)))
			; //checks for transmission complete and TxFifo empty
		__asm__ __volatile__ ("fence");
	}else{
		IIC_REG(id,IIC_REG_CR) = 0x02; //Set Stop bit;
		__asm__ __volatile__ ("fence");
		while (((IIC_REG(id, IIC_REG_SR0)) & IIC_SR0_STPS) != (IIC_SR0_STPS))
			; //check stop sequence initiated
	}

	txBufferIndex = 0;
	txBufferLength = 0;
	// indicate that we are done transmitting
	transmitting = 0;
	return 0;
}

uint8_t TwoWire::endTransmission(void) {
	return endTransmission(true);
}
/**
 @fn size_t TwoWire::write(uint8_t data)
 @brief  writes data
 @details  writes data from a peripheral device in response to a request from a controller device,
 or queues bytes for transmission from a controller to peripheral device
 @param[in] (uint8_t data)-- an array of data to send as bytes.
 @param[Out] No output parameters.
 @return 0 if data size exceeded and return 1 if txBufferLength = txBufferIndex;
 */
size_t TwoWire::write(uint8_t data) {

	if (txBufferIndex >= BUFFER_LENGTH) {
		Serial.print("error, data size exceeded call end transmission() before more write()");
		setWriteError();
		return 0;
	}
// put byte in tx buffer
	txBuffer[txBufferIndex] = data;
	++txBufferIndex;
// update amount in buffer
	txBufferLength = txBufferIndex;
	return 1;
}
/**
 @fn size_t TwoWire::write(const uint8_t *data, size_t quantity)
 @brief  writes data
 @details  writes data from a peripheral device in response to a request from a controller device,
 or queues bytes for transmission from a controller to peripheral device
 @param[in] (uint8_t data)-- an array of data to send as bytes.
 @param[Out] No output parameters.
 @return quantity
 */
size_t TwoWire::write(const uint8_t *data, size_t quantity) {
	for (size_t i = 0; i < quantity; ++i) {
		write(data[i]);
	}
	return quantity;
}
/**
 @fn int TwoWire::available(void)
 @brief  number of bytes available
 @details returns the number of bytes available for retrieval with read()
 @param[in] No Input parameters.
 @param[Out] No output parameters
 @return rxBufferLength
 */
int TwoWire::available(void) {
	return rxBufferLength;
}
/**
 @fn int TwoWire::read(void)
 @brief  reads a byte
 @details reads a byte that was transmitted from a peripheral device to a controller device
 @param[in] No Input parameters.
 @param[Out] No output parameters.
 @return value
 */
int TwoWire::read(void) {

	int value = -1;

	// get each successive byte on each call
	if (rxBufferLength > 0) {
		value = rxBuffer[rxBufferIndex];
		++rxBufferIndex;
		rxBufferLength--;
	}
	return value;
}

/*int TwoWire::peek(void) {

}*/

void TwoWire::flush(void) {
// XXX: to be implemented.
}

void TwoWire::onRequestService(void) {

}

// sets function called on slave write
void TwoWire::onReceive(void (*function)(int)) {
	user_onReceive = function;
}

// sets function called on slave read
void TwoWire::onRequest(void (*function)(void)) {
	user_onRequest = function;
}

// Preinstantiate Objects //////////////////////////////////////////////////////

//TwoWire Wire = TwoWire(0);
```

#### Description
```c++
while (((IIC_REG(id, IIC_REG_SR0)) & (IIC_SR0_STPS)) != IIC_SR0_STPS)
		; //wait for stop bit to be set
```
In requestFrom() function above line is commented as in newer I2C IP, automatic stop bit will not set. 

```c++
	if (sendStop == false) {
		transmitting = 1;
		// reset tx buffer iterator vars
		txBufferIndex = 0;
		txBufferLength = 0;
		IIC_REG(id,IIC_REG_TxCLR) = 0xFF;		//clear TxFifo
		while ((((IIC_REG(id, IIC_REG_SR0)) & IIC_SR0_TxFFE) != (IIC_SR0_TxFFE))
				|| (((IIC_REG(id, IIC_REG_SR0)) & IIC_SR0_TxC) != (IIC_SR0_TxC)))
			; //checks for transmission complete and TxFifo empty
		__asm__ __volatile__ ("fence");
	}else{
		IIC_REG(id,IIC_REG_CR) = 0x02; //Set Stop bit;
		__asm__ __volatile__ ("fence");
		while (((IIC_REG(id, IIC_REG_SR0)) & IIC_SR0_STPS) != (IIC_SR0_STPS))
			; //check stop sequence initiated
	}
```
In endTransmission(), above part of code added for repeated start condition. If parameter passed to endTransmission is `true` (not repeated start) then stop bit will generate and if the parameter is `false` (repeated start), stop bit will not generate.

### 2. platform.h
In this file I2C-0 and I2C-1 base address are changed. Also UART-0 based address is modified.

#### Code
```c++
// See LICENSE for license details.

#ifndef _VEGA_PLATFORM_H
#define _VEGA_PLATFORM_H

#include <stdint.h>

/****************************************************************************
 * Platform definitions
 *****************************************************************************/

#define GPIO_BASE_ADDR 0x10080000
#define GPI1_BASE_ADDR 0x10180000

#define IIC0_BASE_ADDR 0x1070000  //0x1070000  original : 0x10000800
#define IIC1_BASE_ADDR 0x1003000 //0x1003000  original : 0x10000900
#define IIC_ADC_ADDR   0x10001000

#define UART0_BASE_ADDR 0x1001000  //0x1001000  original : 0x10000100
#define UART1_BASE_ADDR 0x10000200
#define UART2_BASE_ADDR 0x10000300

#define SPI0_BASE_ADDR 0x10000600
#define SPI1_BASE_ADDR  0x10000700
#define SPI2_BASE_ADDR 0x10200100
#define SPI3_BASE_ADDR 0x10200200

#define PWM0_BASE_ADDR 0x10400000
#define PWM1_BASE_ADDR 0x10400010
#define PWM2_BASE_ADDR 0x10400020
#define PWM3_BASE_ADDR 0x10400030
#define PWM4_BASE_ADDR 0x10400040
#define PWM5_BASE_ADDR 0x10400050
#define PWM6_BASE_ADDR 0x10400060
#define PWM7_BASE_ADDR 0x10400070

#define TIMER0_BASE_ADDR 0x10000A00
#define TIMER1_BASE_ADDR 0x10000A14
#define TIMER2_BASE_ADDR 0x10000A28

#define TIMERSINTSTATUS_BASE_ADDR 	  0x10000AA0

#define TIMERSEOI_BASE_ADDR 		  0x10000AA4

#define TIMERSRAWINTSTATUS_BASE_ADDR  0x1000_0AA8


//Register offsets
// New
#define UART_LSR_DR			    (1)			//Data Ready
#define UART_LSR_THRE			(1 << 5)    //Transmitter Holding Register
#define UART_LSR_TEMT			(1 << 6)	//Transmitter Empty

// SPI
#define SPIM_CR                         0x00                    // Control Register
#define SPIM_SR                         0x04                    // Status Register
#define SPIM_BRR                        0x08                   // Baud Rate Register
#define SPIM_TDR                        0x0C                    // Transmit Data Register
#define SPIM_RDR                        0x10                   // Receive Data Register

// Helper functions
#define _REG32(p, i) (*((volatile unsigned int *) (p+ i)))
#define _REG8(p, i) (*((volatile unsigned char *) (p+ i)))
#define _REG8P(p, i) (*((volatile unsigned char *) (p+ i)))
#define _REG32P(p, i) (*(volatile unsigned int *) (p + i))

#define GPIO_REG(offset) _REG32(GPIO_BASE_ADDR, offset)

#define IIC0_REG(offset) _REG8(IIC0_BASE_ADDR, offset)
#define IIC1_REG(offset) _REG8(IIC1_BASE_ADDR, offset)
#define IIC8_REG(offset) _REG8(IIC_ADC_ADDR, offset)

#define PWM0_REG(offset) _REG32(PWM0_BASE_ADDR, offset)
#define PWM1_REG(offset) _REG32(PWM1_BASE_ADDR, offset)
#define PWM2_REG(offset) _REG32(PWM2_BASE_ADDR, offset)
#define PWM3_REG(offset) _REG32(PWM3_BASE_ADDR, offset)
#define PWM4_REG(offset) _REG32(PWM4_BASE_ADDR, offset)
#define PWM5_REG(offset) _REG32(PWM5_BASE_ADDR, offset)
#define PWM6_REG(offset) _REG32(PWM6_BASE_ADDR, offset)
#define PWM7_REG(offset) _REG32(PWM7_BASE_ADDR, offset)

#define SPI0_REG(offset) _REG32(SPI0_BASE_ADDR, offset)
#define SPI1_REG(offset) _REG32(SPI1_BASE_ADDR, offset)
#define SPI2_REG(offset) _REG32(SPI2_BASE_ADDR, offset)
#define SPI3_REG(offset) _REG32(SPI3_BASE_ADDR, offset)

#define TIMER0_REG(offset) _REG32(TIMER0_BASE_ADDR, offset)
#define TIMER1_REG(offset) _REG32(TIMER1_BASE_ADDR, offset)
#define TIMER2_REG(offset) _REG32(TIMER2_BASE_ADDR, offset)

#define UART0_REG(offset) _REG32(UART0_BASE_ADDR, offset)
#define UART1_REG(offset) _REG32(UART1_BASE_ADDR, offset)
#define UART2_REG(offset) _REG32(UART2_BASE_ADDR, offset)




#endif /* _VEGA_PLATFORM_H */
```
#### Description
```c++
#define IIC0_BASE_ADDR 0x1070000  //0x1070000  original : 0x10000800
#define IIC1_BASE_ADDR 0x1003000 //0x1003000  original : 0x10000900

#define UART0_BASE_ADDR 0x1001000  //0x1001000  original : 0x10000100
```
I2C-0 & I2C-1 base address for new I2C IP is 0x1070000 & 0x1003000 respectively. 
UART-0 base address is 0x1001000.

### 3. link.lds
Starting address changed to 0x40000 from 0x200000.
```c++
OUTPUT_ARCH( "riscv" )

ENTRY( _start )

MEMORY
{
  ram (wxa!ri) : ORIGIN = 0x40000, LENGTH = 256K
}

PHDRS
{
  ram PT_LOAD;
  ram_init PT_LOAD;
  ram PT_NULL;
}

SECTIONS
{
  __stack_size = 2K;
  __heap_size = 2K;
  

  .init           :
  {
    KEEP (*(SORT_NONE(.init)))
  } >ram AT>ram :ram

  .text           :
  {
    *(.text.unlikely .text.unlikely.*)
    *(.text.startup .text.startup.*)
    *(.text .text.*)
    *(.gnu.linkonce.t.*)
  } >ram AT>ram :ram

  .fini           :
  {
    KEEP (*(SORT_NONE(.fini)))
  } >ram AT>ram :ram

  PROVIDE (__etext = .);
  PROVIDE (_etext = .);
  PROVIDE (etext = .);

  .rodata         :
  {
    *(.rdata)
    *(.rodata .rodata.*)
    *(.gnu.linkonce.r.*)
  } >ram AT>ram :ram

  . = ALIGN(4);

  .preinit_array  :
  {
    PROVIDE_HIDDEN (__preinit_array_start = .);
    KEEP (*(.preinit_array))
    PROVIDE_HIDDEN (__preinit_array_end = .);
  } >ram AT>ram :ram

  .init_array     :
  {
    PROVIDE_HIDDEN (__init_array_start = .);
    KEEP (*(SORT_BY_INIT_PRIORITY(.init_array.*) SORT_BY_INIT_PRIORITY(.ctors.*)))
    KEEP (*(.init_array EXCLUDE_FILE (*crtbegin.o *crtbegin?.o *crtend.o *crtend?.o ) .ctors))
    PROVIDE_HIDDEN (__init_array_end = .);
  } >ram AT>ram :ram

  .fini_array     :
  {
    PROVIDE_HIDDEN (__fini_array_start = .);
    KEEP (*(SORT_BY_INIT_PRIORITY(.fini_array.*) SORT_BY_INIT_PRIORITY(.dtors.*)))
    KEEP (*(.fini_array EXCLUDE_FILE (*crtbegin.o *crtbegin?.o *crtend.o *crtend?.o ) .dtors))
    PROVIDE_HIDDEN (__fini_array_end = .);
  } >ram AT>ram :ram

  .ctors          :
  {
    /* gcc uses crtbegin.o to find the start of
       the constructors, so we make sure it is
       first.  Because this is a wildcard, it
       doesn't matter if the user does not
       actually link against crtbegin.o; the
       linker won't look for a file to match a
       wildcard.  The wildcard also means that it
       doesn't matter which directory crtbegin.o
       is in.  */
    KEEP (*crtbegin.o(.ctors))
    KEEP (*crtbegin?.o(.ctors))
    /* We don't want to include the .ctor section from
       the crtend.o file until after the sorted ctors.
       The .ctor section from the crtend file contains the
       end of ctors marker and it must be last */
    KEEP (*(EXCLUDE_FILE (*crtend.o *crtend?.o ) .ctors))
    KEEP (*(SORT(.ctors.*)))
    KEEP (*(.ctors))
  } >ram AT>ram :ram

  .dtors          :
  {
    KEEP (*crtbegin.o(.dtors))
    KEEP (*crtbegin?.o(.dtors))
    KEEP (*(EXCLUDE_FILE (*crtend.o *crtend?.o ) .dtors))
    KEEP (*(SORT(.dtors.*)))
    KEEP (*(.dtors))
  } >ram AT>ram :ram

  .lalign         :
  {
    . = ALIGN(4);
    PROVIDE( _data_lma = . );
  } >ram AT>ram :ram

  .dalign         :
  {
    . = ALIGN(4);
    PROVIDE( _data = . );
  } >ram AT>ram :ram_init

  .data          :
  {
    *(.data .data.*)
    *(.gnu.linkonce.d.*)
  } >ram AT>ram :ram_init

  .srodata        :
  {
    PROVIDE( _gp = . + 0x800 );
    *(.srodata.cst16)
    *(.srodata.cst8)
    *(.srodata.cst4)
    *(.srodata.cst2)
    *(.srodata .srodata.*)
  } >ram AT>ram :ram_init

  .sdata          :
  {
    *(.sdata .sdata.*)
    *(.gnu.linkonce.s.*)
  } >ram AT>ram :ram_init

  . = ALIGN(4);
  PROVIDE( _edata = . );
  PROVIDE( edata = . );

  PROVIDE( _fbss = . );
  PROVIDE( __bss_start = . );
  .bss            :
  {
    *(.sbss*)
    *(.gnu.linkonce.sb.*)
    *(.bss .bss.*)
    *(.gnu.linkonce.b.*)
    *(COMMON)
    . = ALIGN(4);
  } >ram AT>ram :ram

  . = ALIGN(8);
  PROVIDE( _end = . );
  PROVIDE( end = . );
  
  .heap :
  {
    PROVIDE( _heap_end = (. + __heap_size));
  } >ram AT>ram :ram

  .stack :
  {
    PROVIDE( _sp = (. + __heap_size + __stack_size));
  } >ram AT>ram :ram
}
```

### 4. wiring.c
Multiplication factor to convert `Clock Count` to `Microseconds` changed to 0.025 for 40Mhz system frequency. 
```c++
/*-
 * Copyright (c) 2015 Marko Zec, University of Zagreb
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * $Id$
 */

#include "Arduino.h"
#include "platform.h"
#include "encoding.h"


unsigned long millis() {

  unsigned long long current_clock_count;
  unsigned long long clk_cnt_msb; 
  unsigned long long clk_cnt_lsb;

  clk_cnt_lsb = read_csr(mcycle);
  clk_cnt_msb = read_csr(mcycleh);
  current_clock_count = (clk_cnt_msb << 32) | clk_cnt_lsb;
  return (((current_clock_count) * 0.025)/1000);
}


unsigned long micros(void) {

  unsigned long long current_clock_count;
  unsigned long long clk_cnt_msb; 
  unsigned long long clk_cnt_lsb;

  clk_cnt_lsb = read_csr(mcycle);
  clk_cnt_msb = read_csr(mcycleh);
  current_clock_count = (clk_cnt_msb << 32) | clk_cnt_lsb;
  return (current_clock_count * 0.0101);
}


void delay(unsigned long  ms) {

  asm volatile("mv a5,%0\n\t"
    "li a6,1 \n\t"
    "beq a5,a6,final\n\t"
    "li a6,3 \n\t"
    "bge a5,a6,cycle_loop\n\t"
    "nop\n\t"
    "nop\n\t"
    "bnez a5,loop\n\t"
    "j final\n\t"
    "mul a4,a5,a4\n\t"
    //"csrr a6,mcycle\n\t"
    "bnez a4,loop1\n\t"
    //"addi a5,a5,-1\n\t"
    //"bnez a5,cycle_loop\n\t"
    : : "r"(ms*1000) : "a5" );
    }

void delayMicroseconds(unsigned long us) {

 asm volatile("mv a5,%0\n\t"
	"li a6,1 \n\t"
	"beq a5,a6,final\n\t"
	"li a6,3 \n\t"
	"bge a5,a6,cycle_loop\n\t"
	"loop: addi a5,a5,-1\n\t"
	"nop\n\t"
	"nop\n\t"
	"bnez a5,loop\n\t"
	"j final\n\t"
	"cycle_loop: li a4,33\n\t"
	"mul a4,a5,a4\n\t"
	//"csrr a6,mcycle\n\t"
	"loop1: addi a4,a4,-1\n\t"	
	"bnez a4,loop1\n\t"
	//"addi a5,a5,-1\n\t"
	//"bnez a5,cycle_loop\n\t"
	"final:\n\t"
	: : "r"(us) : "a5" );
}
```

### 5. UARTClass.cpp
Board frequency changed to 40Mhz inside `UARTClass::begin()` fuction.

```c++
/**
 @file UARTClass.cpp
 @brief Contains routines for UART interface
 @detail Includes software functions to initialize,
 configure, transmit and receive over UART
 */

/*  Include section
 *
 ***************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "UARTClass.h"

#include "variant.h"
#include "platform.h"
void *__dso_handle;

/*  Global variable section
 *
 ***************************************************/

UARTClass Serial(0);

/** @fn UARTClass::UARTClass(uint32_t _id) : id(_id)
 @brief Initialize UART Port.
 @details This function initialize the UART port.
 @warning
 @param[in]  unsigned int (_id)--This parameter sets UART Port.
 '0' = UART PORT 0, '1' = UART PORT 1, '2' = UART PORT 2
 @param[Out] No output parameter
 */
UARTClass::UARTClass(uint32_t _id) :
		id(_id) {

}

/** @fn void UARTClass::sio_setbaud(int bauds)
 @brief set baud rate for uart
 @details Calculate Divisor(Divisor = Input frequency / (Baud rate X 16) )
 for the baud rate  and configure uart register. UART frame is initialized by
 setting the data bits,parity bits and stop bits
 8 Data bits, 1 Stop bit, no Parity,Disable DR interrupt & THRE interrupt
 @warning
 @param[in]  int (bauds)--Baud rate
 @param[Out] No output parameter
 @return Void function.
 */

void UARTClass::sio_setbaud(int bauds) {
	unsigned long divisor;
	UART_REG(id, UART_REG_LCR) = 0x83;	//setting DLAB = 1 in LCR
	divisor = (F_CPU / (bauds * 16));
	UART_REG(id, UART_REG_DR) = divisor & 0xFF;	//to get LSB only AND with FF
	UART_REG(id, UART_REG_IE) = (divisor >> 0x08) & 0xFF;	//to get MSB only
	UART_REG(id, UART_REG_LCR) = 0x03;	//Clearing DLAB in LCR
	UART_REG(id, UART_REG_IE) = 0x00;
}

/** @fn void UARTClass::begin( unsigned long bauds)
 @brief Initialize UART
 @details Initialise UART with the customized setting
 @warning
 @param[in]  unsigned long (bauds)--Baud rate
 @param[Out] No output parameter
 @return Void function
 */
void UARTClass::begin(unsigned long bauds) {
	unsigned long divisor;
	divisor = (40000000 / (bauds * 16));
	UART_REG( id, UART_REG_LCR ) = 0x83; //0x83
	UART_REG( id, UART_REG_DR) = divisor & 0xFF;	//LSB//0x28
	UART_REG( id, UART_REG_IE) = (divisor >> 0x08) & 0xFF;	//MSB//0x00
	UART_REG( id, UART_REG_LCR) = 0x03;	//set DLAB to 0
	UART_REG(id, UART_REG_IE) = 0x00;
	UART_REG( id, UART_REG_IIR_FCR) = 0x00;
	__asm__ __volatile__ ("fence");
}

/** @fn int UARTClass::sio_getchar(int c)
 @brief 1 byte character reception
 @details Receives 1 character through uart
 @warning
 @param[in]  int (c) --The variable to hold the read character
 @param[Out] No output parameter
 @return c -- read character
 */
int UARTClass::sio_getchar(int c) //c-The variable to hold the read character
		{
	while ((UART_REG(id, UART_REG_LSR) & UART_LSR_DR) != UART_LSR_DR)
		;  //waiting for Data Ready
	c = UART_REG(id, UART_REG_DR);
	return c;
}

/** @fn int UARTClass::sio_putchar(char c)
 @brief 1 byte character transmission
 @details Transmit 1 character through uart.Proceeds only when transmitter is idle
 @warning
 @param[in]  char (c) --The variable to write character
 @param[Out] No output parameter
 @return 0
 */
int UARTClass::sio_putchar(char c) {
	while ((UART_REG(id, UART_REG_LSR) & UART_LSR_THRE) != UART_LSR_THRE)
		;	//waiting for THRE to be empty
	UART_REG(id,UART_REG_DR) = c;
	__asm__ __volatile__ ("fence");
	return 0;
}

/** @fn int UARTClass::available(void)
 @brief to check availability of data
 @details Get the number of bytes (characters) available for reading from the serial port.
 @warning
 @param[in]  No input parameter
 @param[Out] No output parameter
 @return The number of bytes available to read.
 */

int UARTClass::available(void) {
	if ((UART_REG(id,UART_REG_LSR) & UART_LSR_DR))
		return 1;
	else
		return 0;
}

/** @fn int UARTClass::availableForWrite(void)
 @brief number of bytes available for writing
 @details Get the number of bytes (characters) available for writing in the
 serial buffer without blocking the write operation.
 @warning
 @param[in]  No input parameter
 @param[Out] No output parameter
 @return void function
 */
/*
 int UARTClass::availableForWrite(void) {
 int busy;
 if (UART_REG(UART_LSR_TEMT) != 0)
 ;
 return (UART_REG(UART_REG_DR));
 }
 */

/** @fn int UARTClass::read(void)
 @brief read data from uart
 @details 1 byte character reception
 @warning
 @param[in]  No input parameter
 @param[Out] No output parameter
 @return int function
 */
int UARTClass::read(void) {

	if ((UART_REG(id, UART_REG_LSR) & UART_LSR_DR) != UART_LSR_DR) {
		return -1; //No data
	}

	int c = UART_REG(id, UART_REG_DR);
	return c;

}

/** @fn size_t UARTClass::write(const uint8_t uc_data)
 @brief write data
 @details
 @warning
 @param[in]  const uint8_t (uc_data)
 @param[Out] No output parameter
 @return 1
 */
size_t UARTClass::write(const uint8_t uc_data) {
	sio_putchar(uc_data);
	return (1);
}

void UARTClass::enableInterrupt(uint8_t tx_intr, uint8_t rx_intr) {

	UART_REG(id, UART_REG_IE) = ((tx_intr << 1) | (rx_intr));
}

void UARTClass::disableInterrupt(void) {

	UART_REG(id, UART_REG_IIR_FCR);
}

void UARTClass::end(void){}
int UARTClass::peek(void){
	return 1;
}
```
